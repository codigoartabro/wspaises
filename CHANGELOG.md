# CHANGELOG
* v.1.0.1.20210217
    * Hotfix - Correccion do nome da conexion jdbc.
* v.1.0.0
    * Primeiro lanzamento.
* v.0.0.2.20210212
    * DAO e configuracion do Servicio.
    * Clases Cidade e Pais.
    * Inicio do Proxecto.