package org.artabro.codigo.jsp.services.modelo;

/**
 * @author laegnur
 *
 */
public class Pais
{
    private String codigo;
    private String nome;

    public Pais(String codigo, String nome)
    {
        this.codigo = codigo;
        this.nome = nome;
    }

    /**
     * @return the codigo
     */
    public String getCodigo()
    {
        return codigo;
    }

    /**
     * @return the nome
     */
    public String getNome()
    {
        return nome;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome)
    {
        this.nome = nome;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj != null && obj instanceof Pais)
        {
            Pais otro = (Pais)obj;
            return this.codigo.equals(otro.codigo);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString()
    {
        return "Pais [codigo=" + codigo + ", nome=" + nome + "]";
    }
}
