package org.artabro.codigo.jsp.services.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.artabro.codigo.jsp.services.modelo.Cidade;
import org.artabro.codigo.jsp.services.modelo.Pais;

/**
 * @author laegnur
 *
 */
public class ServicioDAO
{
    private DataSource ds;
    private static final String BUSCAR_PAIS = 
        "SELECT * FROM `tb_paises`;";
    private static final String BUSCAR_CIDADE = 
        "SELECT * FROM `tb_cidades` JOIN `tb_paises` ON `codigoPais` = `codigo`\n"
        + "WHERE `codigoPais` = ?;";

    /**
     * Constructor Baleiro
     */
    public ServicioDAO()
    {
        try
        {
            Context contexto = new InitialContext();
            this.ds = (DataSource)contexto.lookup("java:comp/env/jdbc/paises");
        }
        catch (NamingException e)
        {
            e.printStackTrace();
            System.out.println("Fallo en el sistema de nombrado");
        }
    }

    /**
     * 
     * @return List<Pais> paises
     * @throws SQLException
     */
    public List<Pais> queryPaises() throws SQLException
    {
        Connection conexion = this.ds.getConnection();
        Statement sentencia = conexion.
            createStatement();
        ResultSet resultado = sentencia.executeQuery(BUSCAR_PAIS);
        List<Pais> paises = new ArrayList<Pais>();
        while(resultado.next())
        {
            String codigoPais = resultado.getString("codigo");
            String nombrePais = resultado.getString("pais");
            Pais pais =  new Pais(codigoPais, nombrePais);
            paises.add(pais);
        }        
        return paises;
    }

    /**
     * 
     * @param codPais
     * @return List<Cidade> cidades
     * @throws SQLException
     */
    public List<Cidade> queryCidades(String codPais) throws SQLException
    {
        Connection conexion = this.ds.getConnection();
        PreparedStatement sentencia = conexion.
            prepareStatement(BUSCAR_CIDADE);
        sentencia.setString(1, codPais);
        ResultSet resultado = sentencia.executeQuery();
        List<Cidade> cidades = new ArrayList<Cidade>();
        while(resultado.next())
        {
            int idCidade = resultado.getInt("idCidade");
            String nomeCidade = resultado.getString("cidade");
            String nomePais = resultado.getString("pais");
            Pais pais = new Pais(codPais, nomePais);
            Cidade cidade = new Cidade(idCidade, nomeCidade, pais);
            cidades.add(cidade);
        }
        return cidades;
    }
}
