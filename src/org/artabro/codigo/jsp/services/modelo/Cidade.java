package org.artabro.codigo.jsp.services.modelo;

/**
 * @author laegnur
 *
 */
public class Cidade
{
    private int codigo;
    private String nome;
    private Pais pais;

    public Cidade(int codigo, String nome, Pais pais)
    {
        this.codigo = codigo;
        this.nome = nome;
        this.pais = pais;
    }

    /**
     * @return the codigo
     */
    public int getCodigo()
    {
        return codigo;
    }

    /**
     * @return the nome
     */
    public String getNome()
    {
        return nome;
    }

    /**
     * @return the pais
     */
    public Pais getPais()
    {
        return pais;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo)
    {
        this.codigo = codigo;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome)
    {
        this.nome = nome;
    }

    /**
     * @param pais the pais to set
     */
    public void setPais(Pais pais)
    {
        this.pais = pais;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj != null && obj instanceof Cidade)
        {
            Cidade otro = (Cidade)obj;
            return this.codigo == otro.codigo;
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString()
    {
        return "Cidade [codigo=" + codigo + ", nome=" + nome + ", pais="
            + pais + "]";
    }
}
