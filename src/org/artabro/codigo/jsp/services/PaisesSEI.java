package org.artabro.codigo.jsp.services;

import java.sql.SQLException;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.artabro.codigo.jsp.services.modelo.Cidade;
import org.artabro.codigo.jsp.services.modelo.Pais;
import org.artabro.codigo.jsp.services.modelo.dao.ServicioDAO;

/**
 * @author laegnur
 *
 */
@WebService
public class PaisesSEI
{
    @WebMethod
    public List<Pais> listarPaises()
    {
        List<Pais> paises = null;
        try
        {
            ServicioDAO dao = new ServicioDAO();
            paises = dao.queryPaises();
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        return paises;
    }

    @WebMethod
    public List<Cidade> listarCidadesPorPais(
        @WebParam(name="codPais")String codPais
    )
    {
        List<Cidade> cidades = null;
        try
        {
            ServicioDAO dao = new ServicioDAO();
            cidades =  dao.queryCidades(codPais);
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        return cidades;
    }
}
