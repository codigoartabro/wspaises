# WebService Paises

Aplicación de proba creada durante o curso "DESENVOLVEMENTO DE APLICACIÓNS CON
TECNOLOXÍAS WEB".

## Configuracion

* ### Database
    Executar o script SQL Script.sql en docs.

* ### Tomcat conf context.xml

    ```
    <Resource name="jdbc/wspaises" auth="Container"
        type="javax.sql.DataSource" maxTotal="100" maxIdle="30"
        maxWaitMillis="10000" username="admin_curso"
        password="<clave>" driverClassName="com.mysql.cj.jdbc.Driver"
        url="jdbc:mysql://localhost:3306/<db_paises>"/>
    ```

## Licencia

[GNU GENERAL PUBLIC LICENSE Version 3](LICENSE)

## Historial de Publicacións

[Changelog](CHANGELOG.md)